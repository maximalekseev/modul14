//
//  ViewController.swift
//  Modul14
//
//  Created by Maxim Alekseev on 21.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var helloLabel: UILabel!
    
    @IBOutlet weak var nameTextfield: UITextField!
    
    @IBOutlet weak var secondNameTextfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        helloLabel.text = "Hello, \(PersistanceUserDefaults.shared.name) \(PersistanceUserDefaults.shared.secondName)!"
        
    }
    
    

    @IBAction func submit(_ sender: UIButton) {
        guard let name = nameTextfield.text, let secondName = secondNameTextfield.text else {        return
        }
        
        PersistanceUserDefaults.shared.name = name
        PersistanceUserDefaults.shared.secondName = secondName
        
        viewDidLoad()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

