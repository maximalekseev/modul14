//
//  PersistanceToDoRealm.swift
//  Modul14
//
//  Created by Maxim Alekseev on 21.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import Foundation
import RealmSwift

class PersistanceToDoRealm {
    
    static let shared = PersistanceToDoRealm ()
    let realm = try! Realm()
    
    func writeTask(task: Task) {
        try! realm.write {
            realm.add (task)
        }
        
    }
        
    func eraseTask(task: Task) {
        
        try! realm.write {
            realm.delete(task)
                    }

    }
    
}

class Task: Object {
    @objc dynamic var taskToDo: String?
}
