//
//  PersistanceUserDefaults.swift
//  Modul14
//
//  Created by Maxim Alekseev on 21.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import Foundation

class PersistanceUserDefaults {
    
    static let shared = PersistanceUserDefaults()
    
    private let kNameKey = "PersistanceUserDefaults.kNameKey"
    private let kSecondNameKey = "PersistanceUserDefaults.kSecondNameKey"
    
    var name : String {
        set { UserDefaults.standard.set(newValue, forKey: kNameKey) }
        get {return UserDefaults.standard.string(forKey: kNameKey) ?? "somebody" }
    }
    
    var secondName: String {
        set { UserDefaults.standard.set(newValue, forKey: kSecondNameKey) }
        get {return UserDefaults.standard.string(forKey: kSecondNameKey) ?? ""}
    }
    
}
