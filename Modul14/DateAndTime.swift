//
//  DateAndTime.swift
//  Modul14
//
//  Created by Maxim Alekseev on 23.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import Foundation

class DateAndTime{
    func resolveDateAndTime (fromdt: Double?) -> String? {
        guard let time = fromdt else {
            return nil
        }
        
        let date = Date(timeIntervalSince1970: time)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.setLocalizedDateFormatFromTemplate("dd MMMM  HH:mm")
        let dateString = dateFormatter.string(from: date)
        
        return dateString
        
    }
}
