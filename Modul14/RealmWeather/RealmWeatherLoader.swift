//
//  RealmWeatherLoader.swift
//  Modul14
//
//  Created by Maxim Alekseev on 23.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import Foundation
import Alamofire
import RealmSwift
//MARK: - Delegates
protocol RealmWeatherLoaderDelegate {
    func loadedWeather (cityWeatherByDayRealm: Results<CityWeatherByDay>)
}
protocol CityWeatherLoaderDelegate {
    func loadedCityWeather(cityWeatherRealm: Results<CityWeather>)
}


// MARK: - Load current weather

class CityWeatherLoader {
    
    var delegate: CityWeatherLoaderDelegate?
    
    func loadCityWeather() {
        let urlString = "https://api.openweathermap.org/data/2.5/weather?id=551487&lang=ru&units=metric&appid=ad739b712b19d9a8679ef3524ea1b949"
        
        guard let url = URL (string: urlString) else { return }
        
        AF.request(url).responseJSON { response in
            
            if let objects = response.value, let jsonDict = objects as? NSDictionary {
                
                let cityWeatherRealm = PersistanceCityWeatherRealm.shared.realm.objects(CityWeather.self)
                
                if cityWeatherRealm.isEmpty {
                    PersistanceCityWeatherRealm.shared.write (weatherJson: jsonDict)
                } else {
                    PersistanceCityWeatherRealm.shared.erase()
                    PersistanceCityWeatherRealm.shared.write (weatherJson: jsonDict)
                }
                DispatchQueue.main.async {
                    let cityWeatherRealm = PersistanceCityWeatherRealm.shared.realm.objects(CityWeather.self)
                    self.delegate?.loadedCityWeather(cityWeatherRealm: cityWeatherRealm)
                }
            }
        }
    }
}

//MARK:- Load weather forecst by 5 days every 3 hours


class RealmWeatherLoader {
    
    var delegate: RealmWeatherLoaderDelegate?
    
    func loadWeather() {
        
        let urlString = "https://api.openweathermap.org/data/2.5/forecast?id=551487&lang=ru&units=metric&appid=ad739b712b19d9a8679ef3524ea1b949"
        guard let url = URL (string: urlString) else { return }
        
        AF.request(url).responseJSON {response in
            
            if let objects = response.value, let jsonDict = objects as? NSDictionary {
                if let sourceArray = jsonDict["list"] as? NSArray {
                    PersistanceCityWeatherByDayRealm().erase()
                    for  value in sourceArray where value is NSDictionary {
                        
                        PersistanceCityWeatherByDayRealm().write(weatherJson: value as! NSDictionary)
                        
                    }
                    DispatchQueue.main.async {
                        let cityWeatherByDayRealm = PersistanceCityWeatherByDayRealm.shared.realm.objects(CityWeatherByDay.self)
                        self.delegate?.loadedWeather(cityWeatherByDayRealm: cityWeatherByDayRealm)
                    }
                    
                }
            }
        }
    }
    
}
