//
//  RealmWeather.swift
//  Modul14
//
//  Created by Maxim Alekseev on 23.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import Foundation
import RealmSwift

//MARK: - Storage model weather

// For current weather

class CityWeather: Object {
    
    @objc dynamic var cityName: String = "City name"
    @objc dynamic var weatherCondition: String = "--"
    @objc dynamic var temp: String = "--"
    @objc dynamic var icon: String = "01d"
}

// For weather forecast by 5 days every 3 hours

class CityWeatherByDay: Object {
    
    
    @objc dynamic var temp: String = ""
    @objc dynamic var date: String = ""
    @objc dynamic var icon: String = ""
    
    
}

//MARK: - Write and clear weather data

// For current weather

class PersistanceCityWeatherRealm {
    
    
    static let shared = PersistanceCityWeatherRealm ()
    let realm = try! Realm()
    
    func erase() {
        let allCityWeather = realm.objects(CityWeather.self).first!
        try! realm.write {
            realm.delete(allCityWeather)
        }
    }
    func write (weatherJson: NSDictionary) {
        guard let cityName = weatherJson["name"] as? String,
            let main = weatherJson["main"] as? NSDictionary,
            let weather = weatherJson["weather"] as? NSArray,
            let temperature = main["temp"] as? Double
            else { return }
        guard let zero = weather[0] as? NSDictionary else { return }
        guard let weatherCondition = zero ["description"] as? String  else { return }
        guard let icon = zero["icon"] as? String else { return }
        
        try! realm.write {
            let realmCityWeather = CityWeather()
            realmCityWeather.cityName = cityName
            realmCityWeather.temp = String(Int(round(temperature)))
            realmCityWeather.icon = icon
            realmCityWeather.weatherCondition = weatherCondition
            realm.add(realmCityWeather)
            
        }
    }
}


// For weather forecast by 5 days every 3 hours

class PersistanceCityWeatherByDayRealm {
    static let shared = PersistanceCityWeatherRealm ()
    let realm = try! Realm()
    
    func erase() {
        let allCityWeather = realm.objects(CityWeatherByDay.self)
        try! realm.write {
            realm.delete(allCityWeather)
        }
    }
    
    func write(weatherJson: NSDictionary) {
        guard let main = weatherJson["main"] as? NSDictionary,
            let weather = weatherJson["weather"] as? NSArray,
            let date = weatherJson["dt"] as? Double
            else { return }
        guard let temperature = main["temp"] as? Double else { return }
        guard let zero = weather[0] as? NSDictionary else { return }
        guard let icon = zero["icon"] as? String else { return }
        try! realm.write {
            let realmCityWeatherByDay = CityWeatherByDay()
            realmCityWeatherByDay.date = DateAndTime().resolveDateAndTime(fromdt: date)!
            realmCityWeatherByDay.temp = String(Int(round(temperature)))
            realmCityWeatherByDay.icon = icon
            
            realm.add(realmCityWeatherByDay)
        }
    }
}
