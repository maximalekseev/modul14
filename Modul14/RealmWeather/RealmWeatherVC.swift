//
//  RealmWeatherVC.swift
//  Modul14
//
//  Created by Maxim Alekseev on 23.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class RealmWeatherVC: UIViewController {
    
    var cityWeatherRealm = PersistanceCityWeatherRealm.shared.realm.objects(CityWeather.self)
    
    var cityWeatherByDayRealm = PersistanceCityWeatherByDayRealm.shared.realm.objects(CityWeatherByDay.self)
    
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var conditionLabel: UILabel!
    
    @IBOutlet weak var conditionImage: UIImageView!
    
    @IBOutlet weak var weatherTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityLabel.textAlignment = .center
        temperatureLabel.textAlignment = .center
        conditionLabel.textAlignment = .center
        
        showCityWeather()
        let cityWeatherLoader = CityWeatherLoader()
        cityWeatherLoader.delegate = self
        cityWeatherLoader.loadCityWeather()
        let realmWeatherLoader = RealmWeatherLoader()
        realmWeatherLoader.delegate = self
        realmWeatherLoader.loadWeather()
        
    }
    
    
    func showCityWeather () {
        guard !cityWeatherRealm.isEmpty else { return }
        
        let cityWeather = cityWeatherRealm[0]
        cityLabel.text = cityWeather.cityName
        temperatureLabel.text = cityWeather.temp + "°C"
        conditionLabel.text = cityWeather.weatherCondition.capitalized
        conditionImage.image = UIImage(named: cityWeather.icon)
        
    }
    
}

extension RealmWeatherVC: UITableViewDelegate, UITableViewDataSource, RealmWeatherLoaderDelegate, CityWeatherLoaderDelegate {
    
    func loadedCityWeather(cityWeatherRealm: Results<CityWeather>) {
        self.cityWeatherRealm = cityWeatherRealm
        showCityWeather()
    }
    
    
    func loadedWeather(cityWeatherByDayRealm: Results<CityWeatherByDay>) {
        
        self.cityWeatherByDayRealm = cityWeatherByDayRealm
        weatherTableView.reloadData()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cityWeatherByDayRealm.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RealmWeatherCell") as! RealmWeatherTableViewCell
        
        cell.conditionImage.image = UIImage (named: cityWeatherByDayRealm[indexPath.row].icon)
        cell.temperatureLabel.text = cityWeatherByDayRealm[indexPath.row].temp + "°"
        cell.dateLabel.text = cityWeatherByDayRealm[indexPath.row].date
        
        
        return cell
        
    }
    
    
}
