//
//  CDWeatherLoader.swift
//  Modul14
//
//  Created by Maxim Alekseev on 25.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import Foundation
import Alamofire
import CoreData
//MARK: - Delegates

protocol CDCityWeatherLoaderDelegate {
    func weatherLoaded (cityWeatherCD: [CityWeatherCD])
}

protocol CDWeatherLoaderDelegate {
    func weatherByDayLoaded(cityWeatherByDayCD: [CityWeatherByDayCD])
}

// MARK: - Load save and clear current weather

class CDCityWeatherLoader {
    
    var delegate: CDCityWeatherLoaderDelegate?
    
    //     Load weather from  OpenWeather
    
    func loadCityWeather() {
        let urlString = "https://api.openweathermap.org/data/2.5/weather?id=551487&lang=ru&units=metric&appid=ad739b712b19d9a8679ef3524ea1b949"
        
        guard let url = URL (string: urlString) else { return }
        
        AF.request(url).responseJSON { response in
            
            if let objects = response.value, let jsonDict = objects as? NSDictionary {
                
                if self.requestCityWeatherArray().isEmpty {
                    
                    self.saveWeather(jsonDict: jsonDict)
                    
                } else {
                    self.clearCityWeather()
                    self.saveWeather(jsonDict: jsonDict)
                }
            }
            
            DispatchQueue.main.async {
                
                self.delegate?.weatherLoaded(cityWeatherCD: self.requestCityWeatherArray())
                
            }
        }
    }
    
    //     Save weather to storage
    
    func saveWeather (jsonDict: NSDictionary) {
        
        guard let cityName = jsonDict["name"] as? String,
            let main = jsonDict["main"] as? NSDictionary,
            let weather = jsonDict["weather"] as? NSArray,
            let temperature = main["temp"] as? Double
            else { return }
        guard let zero = weather[0] as? NSDictionary else { return }
        guard let weatherCondition = zero ["description"] as? String  else { return }
        guard let icon = zero["icon"] as? String else { return }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "CityWeatherCD", in: context)
        let cityWeatherCDObject = NSManagedObject(entity: entity!, insertInto: context) as! CityWeatherCD
        cityWeatherCDObject.cityName = cityName
        cityWeatherCDObject.temp = String(Int(round(temperature)))
        cityWeatherCDObject.weatherConditions = weatherCondition
        cityWeatherCDObject.icon = icon
        
        do {
            
            try context.save()
            
        } catch {
            print (error.localizedDescription)
        }
    }
    
    //     Clear storage data
    
    func clearCityWeather () {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        do {
            try context.execute(NSBatchDeleteRequest(fetchRequest: NSFetchRequest(entityName: "CityWeatherCD")))
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
        
    }
    
    //    Request data from storage
    
    func requestCityWeatherArray () -> [CityWeatherCD] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<CityWeatherCD> = CityWeatherCD.fetchRequest()
        var cityWeatherCD: [CityWeatherCD] = []
        do {
            cityWeatherCD = try context.fetch(fetchRequest)
        } catch {
            print(error.localizedDescription)
        }
        return cityWeatherCD
    }
    
}

//MARK:- Load, save and clear data weather forecst by 5 days every 3 hours

class CDWeatherLoader {
    
    var delegate: CDWeatherLoaderDelegate?
    
    func loadWeather() {
        
        let urlString = "https://api.openweathermap.org/data/2.5/forecast?id=551487&lang=ru&units=metric&appid=ad739b712b19d9a8679ef3524ea1b949"
        guard let url = URL (string: urlString) else { return }
        
        AF.request(url).responseJSON {response in
            
            if let objects = response.value, let jsonDict = objects as? NSDictionary {
                if let sourceArray = jsonDict["list"] as? NSArray {
                    self.clearWeatherForecast()
                    for  value in sourceArray where value is NSDictionary {
                        
                        self.saveWeatherForecast(weatherJson: value as! NSDictionary)
                        
                    }
                    DispatchQueue.main.async {
                        self.delegate?.weatherByDayLoaded(cityWeatherByDayCD: self.requestCityWeatherForecastArray())
                    }
                    
                }
            }
        }
    }
    
    //     Save weather forecast to storage
    
    func saveWeatherForecast(weatherJson: NSDictionary) {
        guard let main = weatherJson["main"] as? NSDictionary,
            let weather = weatherJson["weather"] as? NSArray,
            let date = weatherJson["dt"] as? Double
            else { return }
        guard let temperature = main["temp"] as? Double else { return }
        guard let zero = weather[0] as? NSDictionary else { return }
        guard let icon = zero["icon"] as? String else { return }
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "CityWeatherByDayCD", in: context)
        let cityWeatherByDayCDObject = NSManagedObject(entity: entity!, insertInto: context) as! CityWeatherByDayCD
        cityWeatherByDayCDObject.date = DateAndTime().resolveDateAndTime(fromdt: date)!
        cityWeatherByDayCDObject.temp = String(Int(round(temperature)))
        cityWeatherByDayCDObject.icon = icon
        
        do {
            try context.save()
            
        } catch {
            print (error.localizedDescription)
        }
        
    }
    
    //     Clear storage data
    
    func clearWeatherForecast() {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        do {
            try context.execute(NSBatchDeleteRequest(fetchRequest: NSFetchRequest(entityName: "CityWeatherByDayCD")))
            try context.save()
        } catch {
            print(error.localizedDescription)
        }
    }
    
    //    Request data from storage
    
    func requestCityWeatherForecastArray () -> [CityWeatherByDayCD] {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<CityWeatherByDayCD> = CityWeatherByDayCD.fetchRequest()
        var cityWeatherByDayCD: [CityWeatherByDayCD] = []
        do {
            cityWeatherByDayCD = try context.fetch(fetchRequest)
        } catch {
            print(error.localizedDescription)
        }
        return cityWeatherByDayCD
    }
    
}
