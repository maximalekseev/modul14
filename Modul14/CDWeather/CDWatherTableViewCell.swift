//
//  CDWatherTableViewCell.swift
//  Modul14
//
//  Created by Maxim Alekseev on 24.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import UIKit

class CDWatherTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var temperatureLabel: UILabel!
        
    @IBOutlet weak var conditionsImage: UIImageView!
}
