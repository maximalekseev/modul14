//
//  CDWeatherVC.swift
//  Modul14
//
//  Created by Maxim Alekseev on 24.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import UIKit
import CoreData
class CDWeatherVC: UIViewController {
    
    var cityWeatherCD : [CityWeatherCD] = []
    var cityWeatherByDayCD: [CityWeatherByDayCD] = []
    
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var temperatureLabel: UILabel!
    
    @IBOutlet weak var conditionsLabel: UILabel!
    
    @IBOutlet weak var conditionsImage: UIImageView!
    
    @IBOutlet weak var weatherTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cityWeatherCD = CDCityWeatherLoader().requestCityWeatherArray()
        cityWeatherByDayCD = CDWeatherLoader().requestCityWeatherForecastArray()
        showCityWeatherCD()
        
        let cityWeatherLoaderCD = CDCityWeatherLoader()
        cityWeatherLoaderCD.delegate = self
        cityWeatherLoaderCD.loadCityWeather()
        
        let cDWeatherLoader = CDWeatherLoader()
        cDWeatherLoader.delegate = self
        cDWeatherLoader.loadWeather()
        
    }
    
    
    
    func showCityWeatherCD() {
        guard !cityWeatherCD.isEmpty else { print ("Empty"); return }
        
        guard let cityName = cityWeatherCD.first?.cityName,
            let temp = cityWeatherCD.first?.temp,
            let weatherConditions = cityWeatherCD.first?.weatherConditions,
            let icon = cityWeatherCD.first?.icon
            else { return }
        cityLabel.text = cityName
        temperatureLabel.text = temp + "°C"
        conditionsLabel.text = weatherConditions.capitalized
        conditionsImage.image = UIImage(named: icon)
    }
    
}


extension CDWeatherVC: UITableViewDelegate, UITableViewDataSource, CDCityWeatherLoaderDelegate, CDWeatherLoaderDelegate {
    
    
    func weatherLoaded(cityWeatherCD: [CityWeatherCD]) {
        self.cityWeatherCD = cityWeatherCD
        showCityWeatherCD()
    }
    
    func weatherByDayLoaded(cityWeatherByDayCD: [CityWeatherByDayCD]) {
        self.cityWeatherByDayCD = cityWeatherByDayCD
        self.weatherTableView.reloadData()
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cityWeatherByDayCD.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CDWeatherCell") as! CDWatherTableViewCell
        cell.dateLabel.text = cityWeatherByDayCD[indexPath.row].date
        cell.temperatureLabel.text = cityWeatherByDayCD[indexPath.row].temp
        if let icon = cityWeatherByDayCD[indexPath.row].icon {
            cell.conditionsImage.image = UIImage(named: icon)
        }
        
        return cell
    }
    
    
}
