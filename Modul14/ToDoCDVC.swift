//
//  ToDoCDVC.swift
//  Modul14
//
//  Created by Maxim Alekseev on 22.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import UIKit
import CoreData

class ToDoCDVC: UIViewController {
    
   
    var toDoItems : [ToDoTaskCD] = []
    
    @IBOutlet weak var toDoTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func addTask(_ sender: UIButton) {
        
        let ac = UIAlertController (title: "Add Task", message: "add new task", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) {action in
            let textField = ac.textFields?[0]
            self.saveTask (taskToDo: (textField?.text)!)
            self.toDoTableView.reloadData()
        }
        let cancel = UIAlertAction (title: "Cancel", style: .default, handler: nil)
        ac.addTextField {
            textField in
            
        }
        ac.addAction(ok)
        ac.addAction(cancel)
        present(ac, animated: true, completion: nil)
        
    }
    
    
    func saveTask (taskToDo: String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "ToDoTaskCD", in: context)
        let taskObject = NSManagedObject(entity: entity!, insertInto: context) as! ToDoTaskCD
        taskObject.taskToDo = taskToDo
        
        do {
            
            try context.save()
            toDoItems.append(taskObject)
            
        } catch {
            print (error.localizedDescription)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest: NSFetchRequest<ToDoTaskCD> = ToDoTaskCD.fetchRequest()
        
        do {
            toDoItems = try context.fetch(fetchRequest)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    
}


extension ToDoCDVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return toDoItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CDCell", for: indexPath)
        
        let task = toDoItems[indexPath.row]
        cell.textLabel?.text = task.taskToDo
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let taskToDelete = toDoItems[indexPath.row]
        
        context.delete(taskToDelete)
        
        do {
            try context.save()
            toDoItems.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
           
        } catch let error as NSError {
            print("Error: \(error), description \(error.userInfo)")
        }
    }
}
