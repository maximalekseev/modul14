//
//  ToDoRealmVC.swift
//  Modul14
//
//  Created by Maxim Alekseev on 21.01.2020.
//  Copyright © 2020 Maxim Alekseev. All rights reserved.
//

import UIKit
import RealmSwift

class ToDoRealmVC: UIViewController {
    
    var tasksToDo = PersistanceToDoRealm.shared.realm.objects(Task.self)
    
    @IBOutlet weak var tasksTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func addTask(_ sender: UIButton) {
        
        let ac = UIAlertController (title: "Add Task", message: "add new task", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) {action in
            let textField = ac.textFields?[0]
            let task = Task()
            task.taskToDo = textField?.text
            PersistanceToDoRealm.shared.writeTask(task: task)
            self.tasksTableView.reloadData()
            
        }
        let cancel = UIAlertAction (title: "Cancel", style: .default, handler: nil)
        ac.addTextField {
            textField in
            
        }
        ac.addAction(ok)
        ac.addAction(cancel)
        present(ac, animated: true, completion: nil)
    }
    
}

extension ToDoRealmVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasksToDo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RealmCell", for: indexPath)
        
        let task = tasksToDo[indexPath.row]
        cell.textLabel?.text = task.taskToDo
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        guard editingStyle == .delete else {
            return
        }
        
        PersistanceToDoRealm.shared.eraseTask (task: tasksToDo[indexPath.row])
        
        tableView.deleteRows(at: [indexPath], with: .automatic)
        
        
    }
    
}
